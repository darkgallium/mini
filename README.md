# mini

mini is a minimal init system. 
It was created mainly for educational purposes and also for the sake of reducing my laptop's boot time.

You may use it at your own risk, it is not ready for any serious use (and will probably never be :)).

## Install

1. Clone this repo
2. `cargo build --release`
3. `cp target/release/mini /sbin/`
4. Create your services files if needed (using the files given in svc/ as a template) and put them in /etc/mini
5. Edit the kernel command line and add `init=/sbin/mini`

## Developing

To ease development, I created a Makefile that will automatically compile and deploy latest version of mini on a QEMU VM.
More details to come...
