build:
	su florian -c "cargo build"

setup-nbd:
	modprobe nbd max_part=8
	qemu-nbd --connect=/dev/nbd0 /home/florian/.vm/arch.qcow
	mkdir -p /mnt/arch-mini
	mkdir -p /mnt/arch-mini-boot
	sleep 1
	mount /dev/nbd0p1 /mnt/arch-mini-boot
	mount /dev/nbd0p2 /mnt/arch-mini
	sleep 1

change-boot-mini:
	sed -i 's/quiet/init\=\/root\/mini/' /mnt/arch-mini-boot/grub/grub.cfg

change-boot-systemd:
	sed -i 's/init\=\/root\/mini/quiet/' /mnt/arch-mini-boot/grub/grub.cfg

cleanup-nbd:
	umount /mnt/arch-mini{,-boot}
	qemu-nbd -d /dev/nbd0

vm:
	su florian -c "bash -c 'cd /home/florian/.vm && ./arch-mini.sh'"

copy-mini:
	cp target/debug/mini /mnt/arch-mini/root/

run: build setup-nbd copy-mini change-boot-mini cleanup-nbd vm
run-systemd: build setup-nbd change-boot-systemd copy-mini cleanup-nbd vm
